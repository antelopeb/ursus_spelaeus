u.view([
    "js/components/test.js",
    "templates/index.html",
    "js/stores/names.js"
],function(test, template, names){
    "use strict";
    
    var methods = {
        name : name,
        names : names,
        init : function(){
            // initialize index.js
            console.log("index.js: initializing")
            
            this.render()
            
            var self = this
            u.watch(["names"], function(){
                self.render()
            })

            // resolve here?
            resolve(this)
        },
        render : function(){
            var body = document.querySelector("u-body"),
                temp = u.template(template, this)
            body.innerHTML = ""
            body.appendChild(temp)
        },
        handler : function(e){
            e.preventDefault()
            // trigger a change to Jimmy
            u.trigger("jimmy", [
                "because",
                "he really stinks"
            ])
            this.names.names[0].firstname = "Stinky"
            u.trigger("names", this.names.names)
        },
        changeName : function(e){
            e.preventDefault()
            u.trigger("firstName", "Sally")
        }
    }
    
    return methods
})