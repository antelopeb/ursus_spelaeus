u.component([
    "templates/components/test.html",
    "js/stores/teststore.js"
],function(template, store){
    'use strict';
    var methods = {
        utag : "u-test",
        name : name,
        template : template,
        els : [],
        store : store,
        init : function(){
            console.log("test.js: initializing a component")
            // setup listeners
            var self = this
            u.watch(["jimmy","firstName","time"], function(){
                self.render()
            })
            // resolve here?
            resolve(this)
        },
        render : function(){
            for(var key in this.els){
                var temp = u.template(this.template, this),
                    el = this.els[key]
                el.innerHTML = ""
                el.appendChild(temp)
            }
        },
        createdCallback : function(el){
            // render on createdCallback
            // push the elements into an array
            this.els.push(el)
            this.render()
        },
        detachedCallback : function(el){
            // always clean up your garbage
            for(var key in this.els){
                var oldEl = this.els[key]
                if(oldEl == el){
                    // splice it out
                    this.els.splice(key,1)
                }
            }
        },
        handler : function(e){
            e.preventDefault()
            // not working
            alert("clicked!")
        }
    }
    return methods
})