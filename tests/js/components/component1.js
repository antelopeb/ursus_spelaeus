u.component([
    "templates/components/component1.html"
],function(template){
    'use strict';
    var methods = {
        utag : "u-comp1",
        name : name,
        template : template,
        init : function(){
            console.log("component1.js: initializing a component")
            // resolve here?
            resolve(this)
        },
        render : function(){
            var temp = u.template(this.template, this)
            this.el.innerHTML = ""
            this.el.appendChild(temp)
        },
        createdCallback : function(el){
            this.el = el
            this.render()
        },
        handler : function(e){
            e.preventDefault()
            alert("you're tiny now!")
        }
    }
    return methods
})