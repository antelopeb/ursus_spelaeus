u.view([
    "js/components/test.js",
    "js/components/component1.js",
    "templates/foo.html",
    "js/stores/names.js"
],function(test, component1, template, names){
    "use strict";
    
    var methods = {
        name : name,
        names : names,
        init : function(){
            // initialize index.js
            console.log("foo.js: initializing")
            var body = document.querySelector("u-body"),
                temp = u.template(template, this)
            body.innerHTML = ""
            body.appendChild(temp)
            
            // resolve here?
            resolve(this)
        },
        handler : function(e){
            e.preventDefault()
            alert("clicked")
        }
    }
    
    return methods
})