u.store(function(){
    var data = {
        name : name,
        init : function(){
            // load some data here to test
            console.log("names.js: initializing")
            
            var self = this
            u.watch(["names"], function(){
                console.log("names changed - we could ajax this somewhere now")
                console.log(self.names)
            })
            
            // resolve when done
            resolve(this)
        },
        names : [
            {
                firstname : "Joey",
                lastname : "Smith",
                username : "joey1978"
            },
            {
                firstname : "Jenny",
                lastname : "Smith",
                username : "joeyswife"
            },
            {
                firstname : "Sally",
                lastname : "Smith",
                username : "littlesally"
            },
            {
                firstname : "Jimmy",
                lastname : "Smith",
                username : "littlejimmy"
            }
        ]
    }
    return data
})