u.store([
  "js/stores/names.js"
], function(names){
    var data = {
        name : name,
        init : function(){
            // load some data here to test
            console.log("teststore.js: initializing")
            console.log(names)
            this.time = Date()
            this.timer()
            resolve(this)
        },
        timer : function(){
            var self = this
            setTimeout(function(){
                self.time = Date()
                u.trigger("time", self.time)
                self.timer()
            },1000)
        },
        jimmy : [
            "because",
            "he just is",
            "and I said so"
        ],
        firstName : "Jimmy"
    }
    return data
})
