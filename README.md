Ursus
======

A JS components framework. Extends and uses webcomponents, underscore and es6 fetch to build out a modern application framework. This particular framework is built with the deliberate exclusion of routing. The JS for a page only loads once that page is requested in the browser, thus keeping the download of code at any given time fairly small.

Possible dependencies if you want backwards support of any kind:
@webcomponents/webcomponentsjs
es6-promise
whatwg-fetch

Pieces
--------

Components - the atomic level components that make up the framework. i.e. datepicker, content reader, etc...

Views - the main level views that drive the web app. These are driven by URL locations on a hashbang (/#/home)

Subviews - Used to build out views, these are built of components. Driven by sub-urls on a hashbang (/#/home/todolist)


Conventions
-------

ABSOLUTELY NO SEMICOLONS IN JAVASCRIPT

AMD style dependencies

Components should carry a vanilla flavor look and feel, but skinnable by theme as well.

Components should be templated in the most declarative style possible, and all templates should be *.html.

Do not use the window namespace except for the base framework namespace of "ursus".

No jQuery.

Modernizr okay if needed.

Components should be installable via bower.

There should be a build script that packs it all together into a single package of JS.


Target
------

URL triggers fetching a specific file. File is loaded into memory only when it is called via URL. Default page is index.js but should be configurable.

To invoke Ursus at all, you must include the body on the page. This will look like this:
<u-body></u-body>

To this end, the core functionality of Ursus shouldn't initiate until the u-body is invoked on the page.

All components should be name-spaced to u-*.

Each page should be invoked like this:
u.view([
    "js/dep1",
    "js/dep2"
],function(dep1, dep2){
    var methods = {
        dependencies : [...],
        init : function(){...},
        render : function(){...},
        template : function(){...},
        destroy : function(){...}
    }
    return methods
})
Internally the page is referred to as "this" and all dependencies are placed on that object in the dependencies array. So, the dependency is a string pointing to a JS file and then it is placed in the array in place as an executable. A universal registry is created to prevent redundancy and allow re-use without re-loading. Subviews are registered like this: this.subview.init(). Components are registered as web-components and called declaratively in the templates.

Subviews:
u.subview([], function(){})

Components:
u.component([],function(){})

Stores:
u.store()

Dispatcher:
u.watch("action", callback)
u.trigger("action", data)

TODO's
-------
Make final resolve on components more graceful -- it's an ugly line of text in the init function, and that's terrible. Make it more graceful so that the author doesn't need to include it.

Add in loading state for pages

Force this.body or this.el for pages to equal <u-body> element, like it does in the components.

Flesh out subviews based on components as in foo.html. This needs consideration for loading, as a page with many subviews could cause long loading to get all the scripts and components. Can we lazy load subviews? If so, how can we do that?

Change over to ES6 based module loading

Consider adding in support for shadow DOM on web components
