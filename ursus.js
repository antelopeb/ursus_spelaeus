/*
*   Ursus Spelaeus
*   ==============
*   A javascript framework for you.
*
*   Copyright 2015 © Greg Davis
*/

!function(){
    "use strict";

    // constructor
    var Ursus = function(){
        // public objects as needed

    }

    Ursus.prototype = {
            init : function(){
                // initialize ursus by loading index.js
                console.log("ursus.js: initializing")

                // set up a popstate listener
                window.onpopstate = function(e){
                    priv.loadView()
                }

                if(!this.loaded){
                    // Safari fires this twice on load
                    priv.loadView()
                }
            },
            view : function(deps, obj){
                // load dependencies
                // remap deps if there are none
                if(typeof deps == "function"){
                    obj = deps
                    deps = null
                }
                priv.loader(deps, obj)
            },
            subview : function(obj){
                // TODO: determine if this is needed
            },
            component : function(deps, obj){
                // remap deps if there are none
                if(typeof deps == "function"){
                    obj = deps
                    deps = null
                }
                priv.loader(deps, obj, function(){
                    // add the component to priv.deps
                    var component = obj()
                    // create the element
                    window.customElements.define(component.utag, class extends HTMLElement{
                      constructor(){
                        super()
                      }
                      connectedCallback(){
                        var newFunc = component.createdCallback.bind(priv.deps[component.name])
                        newFunc(this)
                      }
                      detachedCallback(){
                        var newFunc = component.detachedCallback.bind(priv.deps[component.name])
                        newFunc(this)
                      }
                    })
                })
            },
            store : function(deps, obj){
                // initialize the store
                // remap deps if there are none
                if(typeof deps == "function"){
                    obj = deps
                    deps = null
                }

                var data = obj(),
                    keys = Object.keys(data)

                // load dependencies
                priv.loader(deps, obj, function(){
                  for(var key in keys){
                      priv.watchman(keys[key], data.name)
                  }
                })

            },
            watch : function(actions, callback){
                // set the callback onto an internal store
                // user priv.dispatcher.action[callback1, callback2, ... ]
                if(typeof actions != "object"){
                    actions = [actions]
                }
                for(var key in actions){
                    var action = actions[key]
                    if(!priv.dispatcher[action]){
                        priv.dispatcher[action] = []
                    }
                    priv.dispatcher[action].push(callback)
                }
            },
            trigger : function(action, data){
                // trigger an action by firing the callbacks
                // re-bind so "this" refers to the data object??
                for(var key in priv.dispatcher[action]){
                    var toSend = {}
                    toSend.type = action
                    toSend.value = data
                    priv.dispatcher[action][key](toSend)
                }
            },
            template : function(template, binding){
                var newTemplate = priv.template(template, binding)
                return newTemplate
            }
        }
    var priv = {
            deps : {},
            dispatcher : {},
            events : ["click", "change", "focus", "blur", "keydown", "keyup", "keypress", "submit", "success", "cancel"],
            loadView : function(){
                console.log("ursus.js: loading page view")
                // it's a view, scroll to the top
                window.scrollTo(0,0)
                // check the URL and fetch the appropriate page
                var url
                if(window.location.hash == "" && window.location.search != ""){
                  // it's using a query string
                  // get the page string from the query
                  url = priv.getQuery("hash")
                  // check just in case it's not using the query string for the location...
                  if(!url){ url = window.location.hash }
                } else {
                  url = window.location.hash
                }
                var page = "index"
                if(url != ""){
                    // assign the page here
                    page = url.split("/")[1]
                }
                //console.log(page)
                if(this.deps[page] == undefined){
                    fetch("js/"+page+".js")
                    .then(function(res){
                        return res.text()
                    }).then(function(t){
                        var np = new Promise(function(res,rej){
                            var newPage = new Function('name', 'resolve', t)
                            newPage(page, res)
                        })
                    })
                } else {
                    this.deps[page].init()
                }
            },
            template : function(template, binding){
                var newTemplate = _.template(template)(binding)
                // parse into HTML and then go through looking for data-[event]
                var element = document.createElement('div')
                element.innerHTML = newTemplate
                // Parse the events in
                for(var key in this.events){
                    var event = this.events[key],
                        items = element.querySelectorAll("[data-on-"+event+"]")
                    for(var i = 0; i < items.length; i++){
                        this.eventHandler(items[i], event, binding)
                    }

                }
                return element.childNodes[0]
            },
            eventHandler : function(el, type, binding){
                var handle = el.getAttribute("data-on-"+type)
                el.addEventListener(type, function(e){
                    var newFunc = binding[handle].bind(binding)
                    newFunc(e)
                })
            },
            makeName : function(url){
                var name = url.split("/")
                return name[name.length-2]+"_"+name[name.length-1].replace(".", "")
            },
            loader : function(deps, obj, callback){
                if(deps === null){
                    var newFunc = obj()
                    this.deps[newFunc.name] = newFunc
                    if(newFunc.init) newFunc.init()
                    if(typeof callback != "undefined") { callback() }
                } else {
                    var self = this
                   new Promise(function(resolve, reject){
                       self.loadDeps(resolve, reject, deps)
                   }).then(function(deps2){
                       self.loadHandler(deps2, obj)
                       if(typeof callback != "undefined") { callback() }
                   }).catch(function(err){
                       // error handling
                   })
                }
            },
            loadDeps : function(resolve, reject, deps){
                // load in dependencies here
                var promises = []
                for(var key in deps){
                    var dep = deps[key],
                        nam = priv.makeName(dep)
                    if(this.deps[nam] == undefined){
                        var newDep = this.loadIt(dep, nam)
                    } else {
                        var newDep = Promise.resolve(nam)
                    }
                    promises.push(newDep)
                }
                Promise.all(promises)
                .then(function(values){
                    return values
                }).then(function(values2){
                    resolve(values2)
                }).catch(function(err){
                    reject(err)
                })
            },
            loadIt : function(dep, name){
                return new Promise(function(resolve, reject){
                  fetch(dep)
                  .then(function(res){
                      return res.text()
                  }).then(function(body){
                      // res.url not reliable, use an alternative
                      resolve({
                          name : name,
                          url : dep,
                          content : body
                      })
                  })
                })
            },
            loadHandler : function(deps, obj){
                // dependencies received, replace deps with new array
               var newDeps = [],
                   loadPromises = [],
                   self = this
               for(var key in deps){
                   var dep = deps[key],
                        name = ""
                   if(typeof dep != "object"){
                       name = dep
                       dep = this.deps[name]
                   }
                    if(this.deps[dep.name] == undefined){
                        // doesn't exist yet
                       if(dep.url.indexOf("js") != -1){
                           // it's a JS file
                           var newP = new Promise(function(res,rej){
                               var newFunc = new Function('name', 'resolve', dep.content)
                               newFunc(dep.name, res)
                           })
                           loadPromises.push(newP)
                           //name = dep.name
                           //newDeps.push(priv.deps[dep.name])
                        } else {
                            // it's HTML or other text
                            var newP = Promise.resolve(dep.content)
                            loadPromises.push(newP)
                            //newDeps.push(dep.content)
                        }
                    } else {
                        // already exists
                        var newP = Promise.resolve(self.deps[name])
                        loadPromises.push(newP)
                        //newDeps.push(priv.deps[name])
                    }
               }
                Promise.all(loadPromises)
                .then(function(values){
                    var newFunc = obj.apply(null, values)
                    self.deps[newFunc.name] = newFunc
                    if(newFunc.init){
                        newFunc.init()
                    }
                })
            },
            watchman : function(label, name){
                var self = this
                u.watch(label, function(datum){
                    self.deps[name][datum.type] = datum.value
                })
            },
            getQuery : function(str){
              var search = window.location.search.split("?")[1],
                  searches = search.split("&"),
                  queries = {}
              for(let key in searches){
                var pieces = searches[key].split("=")
                queries[pieces[0]] = pieces[1]
              }
              return queries[str]
            }
        }

    window.u = new Ursus()

    // invocation of a u-body custom component
    window.customElements.define('u-body', class extends HTMLElement {
      constructor(){
        super()
      }
      connectedCallback(){
        u.init()
      }
    })

}()
